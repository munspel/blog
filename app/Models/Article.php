<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Article
 * @package App\Models
 * @version October 2, 2019, 4:42 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection categories
 * @property string name
 * @property string short_text
 * @property string text
 * @property integer status
 * @property string author
 */
class Article extends Model
{


    public $table = 'articles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';





    public $fillable = [
        'name',
        'short_text',
        'text',
        'status',
        'author'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'short_text' => 'string',
        'text' => 'string',
        'status' => 'integer',
        'author' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'short_text' => 'required',
        'text' => 'required',
        'status' => 'required',
        'author' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function categories()
    {
        return $this->belongsToMany(\App\Models\Category::class, 'article_category');
    }
}
