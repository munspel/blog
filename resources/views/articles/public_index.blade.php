@extends("layouts.public")
@section("content")
    <div class="container">
        {{ $articles->links() }}
        @foreach ($articles as $article)
            <article>
                <header>
                    <h1>
                        {{ $article->name }}
                    </h1>
                </header>
                <section>
                    {{ $article->short_text }}
                </section>
                <footer>
                    <p><b>Author:</b> {{ $article->author }}</p>
                </footer>
            </article>
            <p>{{ $article->name }}</p>
        @endforeach
        {{ $articles->links() }}
    </div>



@endsection
